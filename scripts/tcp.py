import logging
import multiprocessing

from scripts.ip_validator import check_if_ipv4_is_valid

logging.getLogger("scapy").setLevel(logging.CRITICAL)
from scapy.layers.inet6 import IPv6
from scapy.layers.inet import IP, TCP
from scapy.sendrecv import sr1, sr

THREADS_LIMIT = 150
ALL_PORTS = [1, 65535]


def get_syn_scan(target_ip: str, port: int):
    response = sr1(IP(dst=target_ip) / TCP(dport=port, flags='S'), timeout=0.001, verbose=0)

    if response and response.haslayer(TCP) and response.getlayer(TCP).flags == "SA":
        print(f"SYN packet successful sent to {target_ip}:{port}")
        sr(IP(dst=target_ip) / TCP(dport=port, flags='R'), timeout=0.001, verbose=0)
        return port
    else:
        return False


def get_syn_scan_v6(target_ip: str, port: int):
    response = sr1(IPv6(dst=target_ip) / TCP(dport=port, flags='S'), timeout=0.001, verbose=0)

    if response and response.haslayer(TCP) and response.getlayer(TCP).flags == "SA":
        print(f"SYN packet successful sent to {target_ip}:{port}")
        sr(IPv6(dst=target_ip) / TCP(dport=port, flags='R'), timeout=0.001, verbose=0)
        return port
    else:
        return False


def check_open_ports(ip_address, ports_range=None):
    ports_range = ports_range or ALL_PORTS

    args = [(ip_address, i) for i in range(int(ports_range[0]), int(ports_range[1]))]

    with multiprocessing.Pool(processes=THREADS_LIMIT) as pool:

        if check_if_ipv4_is_valid(ip_address):
            results = pool.starmap(get_syn_scan, args)
        else:
            results = pool.starmap(get_syn_scan_v6, args)

        print('Open ports:', list(filter(bool, results)))
