# argsparser.py

import argparse

from scripts.icmp import ping_server
from scripts.ip_validator import check_if_ip_is_valid
from scripts.tcp import check_open_ports, get_syn_scan


def get_args():
    parser = create_parser()
    namespace, unknown = parser.parse_known_args()
    return namespace


def create_parser():
    parser = argparse.ArgumentParser(
        description="CLI-based IP scanner. Note: on some systems you need to use sudo to run the script",
        epilog="Example: python3 scanner.py 192.168.1.1 -p 1 1024")
    parser.add_argument("ip", metavar="xxx.xxx.xxx.xxx", help="IP (Required)")
    parser.add_argument("-p", "--port", metavar='80', nargs='+', help="Port or range of ports")
    parser.add_argument("-s", "--syn", action='store_true', help="TCP Mode (automatically if port passed)")
    parser.add_argument("-a", "--attempts", metavar='5', help="Attempts to ping the host")
    return parser


def parse_args():
    args = get_args()

    if not check_if_ip_is_valid(args.ip):
        print("Please enter valid IPv4 or IPv6 address")
        exit()

    if not args.port and args.syn:
        check_open_ports(args.ip)
        exit()

    if not args.port and not args.syn:
        ping_server(args.ip, attempts=args.attempts)
        exit()

    if args.port:
        if len(args.port) == 1:
            get_syn_scan(args.ip, int(args.port[0]))
            exit()
        if len(args.port) == 2:
            check_open_ports(args.ip, args.port)
            exit()
        if len(args.port) > 2:
            print("Enter only 1 or range of ports")
            exit()
