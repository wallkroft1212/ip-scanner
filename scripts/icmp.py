# icmp.py
import platform
import subprocess


def ping_server(host, attempts):
    attempts = attempts if attempts else '1'

    param = '-n' if platform.system().lower() == 'windows' else '-c'

    try:
        response = subprocess.run(
            ['ping', param, attempts, host],
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE
        )

        print(response.stdout.decode('utf-8'))
    except Exception as e:
        print(f"An error occurred: {e}")
