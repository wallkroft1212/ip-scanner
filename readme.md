# Python TCP/SYN and ICMP scanner

## Prerequisites

- Python 3.x
- Administrator or root privileges (required for creating raw sockets)

## Setup

### 1. Clone the Repository

```bash
git clone https://gitlab.com/wallkroft1212/ip-scanner
cd ip-scanner
```

### 2. Create a Virtual Environment

It is recommended to use a virtual environment to manage dependencies and avoid conflicts with other projects.

```bash
python3 -m venv venv
```

### 3. Activate the Virtual Environment

On Windows:

```bash
venv\Scripts\activate
```

On macOS and Linux:

```bash
source venv/bin/activate
```

### 4. Install Dependencies

```bash
pip install -r requirements.txt
```

# Running the Script

### 1. Ensure Root Privileges

This script requires root privileges to create raw sockets. On Unix-based systems, run the script with sudo:

```bash
sudo python3 scanner.py
```

### 2. Script Usage

The script pings a list of predefined hosts and displays the ICMP packet details for each response.

```bash
python scanner.py 192.168.1.1 -p 1 1024
```

```bash
python scanner.py ::1 -s
```

```bash
python scanner.py 8.8.8.8 -a 3
```

You can see additional info using

```bash
python scanner.py -h
```
